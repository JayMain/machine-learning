# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/23 
@Time    : 10:36 AM
@Author  : MingJie Li
@Desc    : resnet的配置文件
"""


class Config(object):
    """
    如需修改，请修改一下配置文件
    """
    neurons_num1 = 2048  # 第一层全连接层神经元数量
    neurons_num2 = 1024  # 第二层全连接层神经元数量
    neurons_num3 = 512  # 第三层全连接层神经元数量
    neurons_num4 = 256  # 第四层全连接层神经元数量

    learning_rate = 1e-4  # 学习率
    batch_size = 1000  # 整数，指定进行梯度下降时每个batch包含的样本数。训练时一个batch的样本会被计算一次梯度下降，使目标函数优化一步。
    epochs = 25  # 整数，训练终止时的epoch值，训练将在达到该epoch值时停止，当没有设置initial_epoch时，它就是训练的总轮数，
    predict_num = 10  # 测试数量
