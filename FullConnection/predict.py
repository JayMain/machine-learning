# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/23 
@Time    : 11:14 AM
@Author  : MingJie Li
@Desc    : resnet预测文件
"""
import os
import sys

import numpy as np

object_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(object_path)

from common.plot import draw_threshold
from common.const import base_dir
from .model import get_fullcnn_model
from common.utils import read_split_data
from .config import Config


def get_threshold(min_score: float = 0.4, max_score: float = 0.6, nums: int = 100):
    """
    获取阈值
    :param min_score: 最低分数
    :param max_score: 最高分数
    :param nums: 阈值可供选择的数目
    :return:
    """
    model = get_fullcnn_model()
    # 加载模型
    model.load_weights(f"{base_dir}FullConnection/weights_best_simple_model.hdf5")
    train_data, train_label, test_data, test_label = read_split_data(rate=0)
    y_pred = model.predict(train_data)
    step = (max_score - min_score) / nums
    x = [min_score + i * step for i in range(nums)]
    y = []
    best_threshold = 0.1
    max_correct = 0
    for threshold in x:
        correct_num = 0
        for i in range(len(train_data)):
            if is_correct(y_pred[i], train_label[i], threshold):
                correct_num += 1
        if correct_num > max_correct:
            best_threshold = threshold
            max_correct = correct_num
        y.append(float(correct_num) / len(train_data))
    draw_threshold(root_dir='FullConnection', x=x, y=y)
    return best_threshold


def is_correct(y_pred: np.array, y_true: np.array, threshold: float):
    """
    判断是否预测正确
    :param y_pred: 预测值
    :param y_true: 真实值
    :param threshold: 阈值
    :return:
    """
    return str(np.where(y_pred >= threshold)) == str(np.where(y_true == 1))


def predict():
    """
    预测模型
    :return:
    """
    model = get_fullcnn_model()
    model.load_weights(f"{base_dir}FullConnection/weights_best_simple_model.hdf5")
    data, label = read_split_data(predict=True, predict_num=Config.predict_num)
    y_pred = model.predict(data)
    print("now is choosing threshold , time maybe a little long , please wait patiently........")
    threshold = get_threshold()
    print(f"choose threshold is done!! ,the threshold is {threshold}")
    for i in range(Config.predict_num):
        y_true_predict = []
        for j in range(len(y_pred[i])):
            # 大于0.5的阈值将会设置为1
            if y_pred[i][j] >= threshold:
                y_true_predict.append(1)
            else:
                y_true_predict.append(0)
        print(f"true label:{label[i]},predict label: {y_true_predict}, "
              f"True or False: {str(label[i].tolist()) == str(y_true_predict)}")
