# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/23 
@Time    : 10:35 AM
@Author  : MingJie Li
@Desc    : 模型文件
"""
import os
import sys

object_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(object_path)

from common.const import num_class, img_size
from .config import Config
from tensorflow.keras import layers, models, Sequential
from tensorflow.keras.layers import Dense, Flatten, Dropout, BatchNormalization


def get_fullcnn_model():
    """
    获取full cnn模型
    :return:
    """
    model = Sequential([
        Flatten(),
        Dense(Config.neurons_num1, activation='relu'),
        Dropout(rate=0.125),  # 防止过拟合
        Dense(Config.neurons_num2, activation='relu'),
        Dropout(rate=0.125),  # 防止过拟合
        Dense(Config.neurons_num3, activation='relu'),
        Dropout(rate=0.125),  # 防止过拟合
        Dense(Config.neurons_num4, activation='relu'),
        Dropout(rate=0.125),  # 防止过拟合
        Dense(num_class, activation='sigmoid')
    ])
    model.build(input_shape=(None, img_size, img_size, 1))
    return model
