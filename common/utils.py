# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/18 
@Time    : 2:46 PM
@Author  : MingJie Li
@Desc    : 获取 Wafer_Map_Datasets.npz 文件中的信息
"""
import random
from .const import *
import numpy as np
import tensorflow as tf

root_dir = base_dir + 'Wafer_Map_Datasets.npz'


def read_split_data(root: str = root_dir, rate: float = 0.2, predict: bool = False, predict_num: int = 5):
    """
    读取数据，并划分训练集合和测试集合
    :param predict_num: 预测的数据
    :param predict: 是否进行预测
    :param root: 数据路径
    :param rate: 测试集所占比例
    :return:
    """
    file = np.load(root)  # 加载数据
    # 混合模式晶圆图缺陷数据，0表示空白点，1代表通过电学测试的正常晶粒，2代表未通过电学测试的失效晶粒。数据形状为52×52。
    data = file['arr_0']
    # 混合模式晶圆图缺陷标签，采用one-hot编码，共8维，分别对应晶圆图缺陷的8种基本类型（单一缺陷）
    label = file['arr_1']

    train_data = []  # 存储训练集的所有数据
    train_label = []  # 存储训练集对应标签信息
    test_data = []  # 存储验证集的所有数据
    test_label = []  # 存储验证集对应标签信息

    # 遍历每一种类别，每一种类别取rate
    for item in index_range:
        len = item[1] + 1 - item[0]
        random_range = random.sample(range(item[0], item[1] + 1), int(len * rate))
        for i in range(item[0], item[1] + 1):
            if i in random_range:
                test_data.append(data[i].tolist())
                test_label.append(label[i].tolist())
            else:
                train_data.append(data[i].tolist())
                train_label.append(label[i].tolist())
    if not predict:
        return np.asarray(train_data).reshape(-1, img_size, img_size, 1), \
               np.asarray(train_label), \
               np.asarray(test_data).reshape(-1, img_size, img_size, 1), \
               np.asarray(test_label)
    else:
        random.seed(0)
        data_predict = random.sample(test_data, predict_num)
        random.seed(0)
        label_predict = random.sample(test_label, predict_num)
        return np.asarray(data_predict).reshape(-1, img_size, img_size, 1), \
               np.asarray(label_predict)


def generate_ds(data_root: str,
                batch_size: int = 8,
                val_rate: float = 0.1,
                cache_data: bool = False):
    """
    读取划分数据集，并生成训练集和验证集的迭代器
    :param data_root: 数据根目录
    :param batch_size: 训练使用的batch size
    :param val_rate:  将数据按给定比例划分到验证集
    :param cache_data: 是否缓存数据
    :return:
    """
    train_data, train_label, val_data, val_label = read_split_data(data_root, rate=val_rate)
    AUTOTUNE = tf.data.experimental.AUTOTUNE

    # 配置数据集合参数
    def configure_for_performance(ds,
                                  shuffle_size: int,
                                  shuffle: bool = False,
                                  cache: bool = False):
        if cache:
            ds = ds.cache()  # 读取数据后缓存至内存
        if shuffle:
            ds = ds.shuffle(buffer_size=shuffle_size)  # 打乱数据顺序
        ds = ds.batch(batch_size)  # 指定batch size
        ds = ds.prefetch(buffer_size=AUTOTUNE)  # 在训练的同时提前准备下一个step的数据
        return ds

    train_ds = tf.data.Dataset.from_tensor_slices((tf.constant(train_data),
                                                   tf.constant(train_label)))
    total_train = len(train_data)

    # 创建训练集合的data，label对
    train_ds = configure_for_performance(train_ds, total_train, shuffle=True, cache=cache_data)

    val_ds = tf.data.Dataset.from_tensor_slices((tf.constant(val_data),
                                                 tf.constant(val_label)))
    total_val = len(val_data)
    #  创建测试集合的data，label对
    val_ds = configure_for_performance(val_ds, total_val, cache=False)

    return train_ds, val_ds
