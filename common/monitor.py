# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/22 
@Time    : 4:58 PM
@Author  : MingJie Li
@Desc    : 监控的指标
"""
import tensorflow.keras.backend as K


def precision(y_true, y_pred):
    """
    计算 precision
    :param y_true: 真实值
    :param y_pred: 预测值
    :return:
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """
    计算 recall
    :param y_true: 真实值
    :param y_pred: 预测值
    :return:
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def fbeta_score(y_true, y_pred, beta=1):
    """
    计算 F score
    :param y_true:  真实值
    :param y_pred:  预测值
    :param beta:
    :return:
    """
    if beta < 0:
        raise ValueError('The lowest choosable beta is zero (only precision).')
    if K.sum(K.round(K.clip(y_true, 0, 1))) == 0:
        return 0.0

    p = precision(y_true, y_pred)
    r = recall(y_true, y_pred)
    bb = beta ** 2
    fbeta_score = (1 + bb) * (p * r) / (bb * p + r + K.epsilon())
    return fbeta_score


def fmeasure(y_true, y_pred):
    """
     计算 F score
    :param y_true:  真实值
    :param y_pred: 预测值
    :return:
    """
    return fbeta_score(y_true, y_pred, beta=1)
