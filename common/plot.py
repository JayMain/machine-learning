# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/19 
@Time    : 5:09 PM
@Author  : MingJie Li
@Desc    : matlab画图工具
"""
import matplotlib.pyplot as plt
from .const import base_dir
import pickle
import random
import numpy as np


def randomcolor():
    """
    生成随机颜色
    :return: 颜色字符串
    """
    colorArr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    color = ""
    for i in range(6):
        color += colorArr[random.randint(0, 14)]
    return "#" + color


def draw_threshold(root_dir: str, x: list, y: list):
    """
    画出阈值曲线图
    :param root_dir: 保存路径
    :param x: x值
    :param y: y值
    :return:
    """
    plt.figure()
    plt.plot(x, y)
    plt.title(f'{root_dir} threshold')
    plt.xlabel('threshold')
    plt.ylabel('acc')
    plt.legend()
    plt.savefig(f'{base_dir + root_dir}/threshold.jpg')


def draw_all_model_evaluation(root_dirs: list):
    """
    获取训练历史记录，画出所有模型的指标图像
    :param root_dirs: 模型所在的路径（相对路径）
    :return:
    """
    histories = {}
    epochs = 0
    for root_dir in root_dirs:
        with open(f'{base_dir + root_dir}/history.txt', 'rb') as fp:
            histories[root_dir] = pickle.load(fp)
        epochs = range(len(histories[root_dir]['val_accuracy']))
    # 准确度
    plt.figure()
    for name, history in histories.items():
        plt.plot(epochs, history['val_accuracy'], randomcolor(), label=f'{name} acc')
    plt.title('accuracy')
    plt.xlabel('epoch')
    plt.ylabel('acc')
    plt.legend()
    plt.savefig(f'{base_dir}acc.jpg')

    plt.figure()
    for name, history in histories.items():
        plt.plot(epochs, history['val_fmeasure'], randomcolor(), label=f'{name} f1')
    plt.title('f1 score')
    plt.xlabel('epoch')
    plt.ylabel('f1')
    plt.legend()
    plt.savefig(f'{base_dir}f1.jpg')

    plt.figure()
    for name, history in histories.items():
        plt.plot(epochs, history['val_recall'], randomcolor(), label=f'{name} recall')
    plt.title('recall')
    plt.xlabel('epoch')
    plt.ylabel('recall')
    plt.legend()
    plt.savefig(f'{base_dir}recall.jpg')

    plt.figure()
    for name, history in histories.items():
        plt.plot(epochs, history['val_precision'], randomcolor(), label=f'{name} precision')
    plt.title('precision')
    plt.xlabel('epoch')
    plt.ylabel('precision')
    plt.legend()
    plt.savefig(f'{base_dir}precision.jpg')


def draw_train_history(model_name: str, history, root_dir: str = None, saveImage: bool = False):
    """
    画出单个模型的训练历史记录
    :param model_name: 模型名称
    :param history: 历史记录对象
    :param root_dir: 存储的路径
    :param saveImage: 是否存储图片
    :return:
    """
    epochs = range(len(history.history['accuracy']))
    if saveImage:
        with open(f'{base_dir + root_dir}/history.txt', 'wb') as fp:
            pickle.dump(history.history, fp)
    # 准确度
    plt.figure()
    plt.plot(epochs, history.history['accuracy'], 'b', label='Training acc')
    plt.plot(epochs, history.history['val_accuracy'], 'r', label='Validation acc')
    plt.title(f'{model_name} Training and Test accuracy')
    plt.xlabel('epoch')
    plt.ylabel('acc')
    plt.legend()
    if saveImage:
        plt.savefig(f'{base_dir + root_dir}/acc.jpg')
    plt.figure()
    # 损失值
    plt.plot(epochs, history.history['loss'], 'b', label='Training loss')
    plt.plot(epochs, history.history['val_loss'], 'r', label='Validation loss')
    plt.title(f'{model_name} Training and Test Loss')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend()
    if saveImage:
        plt.savefig(f'{base_dir + root_dir}/loss.jpg')

    plt.figure()
    plt.plot(epochs, history.history['fmeasure'], 'b', label='Training f1')
    plt.plot(epochs, history.history['val_fmeasure'], 'r', label='Validation f1')
    plt.title(f'{model_name} Training and Test f1')
    plt.xlabel('epoch')
    plt.ylabel('f1')
    plt.legend()
    if saveImage:
        plt.savefig(f'{base_dir + root_dir}/f1.jpg')

    plt.figure()
    plt.plot(epochs, history.history['recall'], 'b', label='Training recall')
    plt.plot(epochs, history.history['val_recall'], 'r', label='Validation recall')
    plt.title(f'{model_name} Training and Test recall')
    plt.xlabel('epoch')
    plt.ylabel('recall')
    plt.legend()
    if saveImage:
        plt.savefig(f'{base_dir + root_dir}/recall.jpg')

    plt.figure()
    plt.plot(epochs, history.history['precision'], 'b', label='Training precision')
    plt.plot(epochs, history.history['val_precision'], 'r', label='Validation precision')
    plt.title(f'{model_name} Training and Test precision')
    plt.xlabel('epoch')
    plt.ylabel('precision')
    plt.legend()
    if saveImage:
        plt.savefig(f'{base_dir + root_dir}/precision.jpg')


def draw_data_preview():
    """
    打印data label标签范围
    :return:
    """
    file = np.load('../Wafer_Map_Datasets.npz')  # 加载数据
    # 混合模式晶圆图缺陷标签，采用one-hot编码，共8维，分别对应晶圆图缺陷的8种基本类型（单一缺陷）
    labels = file['arr_1']
    label_num = [0] * 8
    for label in labels:
        indexs = np.where(label == 1)[0]
        for index in indexs:
            label_num[index] += 1
    plt.figure()
    plt.bar(range(1, 9), label_num)
    plt.title('label distribution')
    plt.xlabel('label')
    plt.ylabel('nums')
    plt.legend()
    plt.savefig(f'label.jpg')


if __name__ == '__main__':
    draw_data_preview()
