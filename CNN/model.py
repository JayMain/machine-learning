# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/19 
@Time    : 10:44 AM
@Author  : MingJie Li
@Desc    : CNN的模型文件
"""
import os
import sys

object_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(object_path)

import tensorflow as tf
from common.const import num_class, img_size
from .config import Config


def get_cnn_model():
    """
    获取cnn实验模型
    :return:
    """
    model = tf.keras.Sequential([
        tf.keras.layers.Conv2D(filters=Config.filter_num1,
                               kernel_size=Config.filter_size1,
                               activation='relu',
                               padding='valid',
                               strides=(2, 2),
                               kernel_initializer='he_normal',
                               kernel_regularizer=tf.keras.regularizers.l2(0.0005),
                               # kernel_initializer=tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=0.05),
                               input_shape=(img_size, img_size, Config.num_channels), ),
        # tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
        tf.keras.layers.Conv2D(filters=Config.filter_num2,
                               kernel_size=Config.filter_size2,
                               activation='relu',
                               kernel_regularizer=tf.keras.regularizers.l2(0.0005),
                               kernel_initializer='he_normal',
                               padding='valid',
                               strides=(2, 2),
                               # kernel_initializer=tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=0.05),
                               ),
        # tf.keras.layers.MaxPooling2D(pool_size=(3, 3)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(Config.fc_size1, activation='relu'),
        tf.keras.layers.Dropout(rate=0.2),  # 防止过拟合
        tf.keras.layers.Dense(Config.fc_size2, activation='relu'),
        tf.keras.layers.Dropout(rate=0.2),  # 防止过拟合
        tf.keras.layers.Dense(num_class, activation='sigmoid')
    ])
    return model
