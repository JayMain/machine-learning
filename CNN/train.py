# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/19 
@Time    : 11:06 AM
@Author  : MingJie Li
@Desc    : CNN训练文件
"""
import os
import sys

object_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(object_path)

from .model import get_cnn_model
import tensorflow as tf
from common.plot import draw_train_history
from common.const import base_dir
from .config import Config
from common.monitor import *


def train(train_data, train_label, test_data, test_label):
    """
    训练
    :param train_data: 训练数据
    :param train_label: 训练标签
    :param test_data: 测试数据
    :param test_label: 测试标签
    :return:
    """
    model = get_cnn_model()
    model.summary()
    # 训练时的回调函数
    checkpointer = tf.keras.callbacks.ModelCheckpoint(filepath=f'{base_dir}CNN/weights_best_simple_model.hdf5',
                                                      monitor='val_fmeasure', verbose=1, save_best_only=True,
                                                      mode='max')
    reduce = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_fmeasure', factor=0.5, patience=2,
                                                  verbose=1, min_delta=1e-4, mode='max')
    model.compile(loss='binary_crossentropy', optimizer=tf.keras.optimizers.Adam(learning_rate=Config.learning_rate),
                  metrics=['accuracy', fmeasure, recall, precision])
    history = model.fit(train_data, train_label, batch_size=Config.batch_size,
                        epochs=Config.epochs, verbose=1, shuffle=True,
                        validation_data=(test_data, test_label),
                        # validation_split=0.2,
                        callbacks=[checkpointer, reduce], )

    draw_train_history(history=history, model_name='CNN', saveImage=True, root_dir='CNN')
    # model.save(f'{base_dir}CNN/cnn.h5')
