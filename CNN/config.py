# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/22 
@Time    : 5:02 PM
@Author  : MingJie Li
@Desc    : config配置文件设置
"""


class Config(object):
    """
    如需修改，请修改一下配置文件
    """
    # 第一层.
    filter_num1 = 3  # 卷积过滤器的数量,对应输出的维数--卷积核的数目（即输出的维度）
    filter_size1 = 4  # 过滤器的大小,如果为一个整数则宽和高相同.单个整数或由两个整数构成的list/tuple，卷积核的宽度和长度。

    # 第二层.
    filter_num2 = 3  # 卷积过滤器的数量,对应输出的维数--卷积核的数目（即输出的维度）
    filter_size2 = 4  # 过滤器的大小,如果为一个整数则宽和高相同.单个整数或由两个整数构成的list/tuple，卷积核的宽度和长度。

    # 第一层全连接层
    fc_size1 = 1024  # 全连接层神经元数目

    # 第二层全连接层
    fc_size2 = 512  # 全连接层神经元数目

    num_channels = 1  # 颜色通道数目
    learning_rate = 1e-4  # 学习率
    batch_size = 1000  # 整数，指定进行梯度下降时每个batch包含的样本数。训练时一个batch的样本会被计算一次梯度下降，使目标函数优化一步。
    epochs = 25  # 整数，训练终止时的epoch值，训练将在达到该epoch值时停止，当没有设置initial_epoch时，它就是训练的总轮数，

    predict_num = 10  # 测试数量
