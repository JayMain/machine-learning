# 面向计算机视觉——机器学习作业

1913090 —— 黎明杰

## 编译环境

>IDE: Pycharm 2021.2
> 
>Python :3.8
>
>OS: Mac BigSur 14.1 

## 运行与编译

### 1. 代码目录克隆
```bash
git clone https://gitlab.com/JayMain/machine-learning.git
```

### 2.数据预处理

* 数据链接： https://tianchi.aliyun.com/dataset/dataDetail?dataId=77328
* 下载解压，将数据放在根目录下。如`***/machine-learning/Wafer_Map_Datasets.npz`

### 3. 修改配置文件

* 在`machine-learning/common/const.py`文件中找到`base_dir`变量，将其修改为自己的项目绝对路径（**后面需要带个/或者\\**)

### 4. 安装编译环境

```bash
pip install -r requirements.txt
```

### 5. 训练和预测

```bash
python main.py
```

## 项目文件分析

```java
├── CNN
│   ├── __init__.py 
│   ├── config.py   // CNN模型训练配置文件，可以从该文件中修改cnn模型的参数
│   ├── model.py    // CNN模型的生成文件
│   ├── predict.py  // CNN 模型的预测文件
│   └── train.py    // CNN模型的训练文件
├── FullConnection
│   ├── __init__.py
│   ├── config.py  // 全连接模型训练配置文件，可以从该文件中修改cnn模型的参数
│   ├── model.py    // 全连接模型的生成文件
│   ├── predict.py  // 全连接模型的预测文件
│   └── train.py   // 全连接模型的训练文件
├── README.md
├── Wafer_Map_Datasets.npz   //数据集文件
├── common
│   ├── __init__.py
│   ├── const.py   // 本次项目的常量变量文件
│   ├── monitor.py // 项目的评价指标函数生成文件
│   ├── plot.py   // 绘画文件
│   └── utils.py   // 工具类，生成测试集合训练集
├── main.py
└── requirements.txt // python需要的环境
```
