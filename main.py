# -*- coding: utf-8 -*-
"""
@Date    : 2022/6/22 
@Time    : 5:00 PM
@Author  : MingJie Li
@Desc    : 启动全部的训练模型
"""
from CNN.train import train as cnn_train
from CNN.predict import predict as cnn_pre
from FullConnection.train import train as resnet_train
from FullConnection.predict import predict as resnet_pre
from common.utils import read_split_data
from common.plot import draw_all_model_evaluation


def main():
    """
    主函数
    :return:
    """
    train_data, train_label, test_data, test_label = read_split_data(rate=0.2)
    print(f"train data shape:{train_data.shape}, train label shape:{train_label.shape}")
    print(f"test data shape:{test_data.shape}, test label shape:{test_label.shape}")
    print('=' * 20 + ' CNN training ' + '=' * 20)
    cnn_train(train_data, train_label, test_data, test_label)
    print('=' * 20 + ' CNN predict ' + '=' * 20)
    cnn_pre()
    print('=' * 20 + ' full connect training ' + '=' * 20)
    resnet_train(train_data, train_label, test_data, test_label)
    print('=' * 20 + ' full connect predict ' + '=' * 20)
    resnet_pre()
    draw_all_model_evaluation(root_dirs=['CNN', 'FullConnection'])


if __name__ == '__main__':
    main()
